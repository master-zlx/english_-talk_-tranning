package com.english;

import com.auth0.jwt.JWT;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.english.domain.User;
import com.english.mappers.RecordMapper;
import com.english.mappers.StudentMapper;
import com.english.mappers.UserMapper;
import com.english.service.ResourceService;
import com.english.service.impl.RankServiceImp;
import com.english.service.impl.UserServiceImp;
import jakarta.annotation.Resource;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
class EnglishApplicationTests {


    //测试数据库是否连通
    @Resource
    StudentMapper studentMapper;
    @Resource
    RecordMapper recordMapper;
    @Resource
    ResourceService resourceService;
    @Resource
    RankServiceImp rankServiceImp;
    @Resource
    UserMapper userMapper;
    @Test
    void contextLoads() {
    }

    //测试标题图片的显示

    @Test
    public void getTextByCnt(){
        System.out.println("文章打卡次数排名(前10):" + rankServiceImp.getTextByCnt());
        System.out.println("学院打卡次数排名(前10):" + rankServiceImp.getDeptByCnt());
        System.out.println("专业打卡次数排名(前10):" + rankServiceImp.getMajorByCnt());
        System.out.println("班级打卡次数排名(前10):" + rankServiceImp.getClassByCnt());
        System.out.println("年级打卡次数排名(前10):" + rankServiceImp.getGradeByCnt());
        System.out.println("文章平均得分排名(前10):" + rankServiceImp.getTextByScore());
        System.out.println("院系平均得分排名(前10)" + rankServiceImp.getDeptByScore());
        System.out.println("专业平均得分排名(前10)" + rankServiceImp.getMajorByScore());
        System.out.println("班级平均得分排名(前10)" + rankServiceImp.getClassByScore());
        System.out.println("年级平均得分排名(前10)" + rankServiceImp.getGradeByScore());
    }

    @Resource
    UserServiceImp userServiceImp;
    @Test
    public void TestY(){
        System.err.println( JWT.decode("eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJhdWQiOiIwIiwiZXhwIjoxNjg4NzU0MzY0fQ.G-PsXi1nr-xrA0RJodA4ujtqx3OhK9pTIBtAFzQPdgE").getAudience().get(0) );
    }

    @Test
    public void TestCnt(){
        System.err.println(rankServiceImp.getCntOfDept("计算机学院"));
        System.err.println(rankServiceImp.getCntOfMajor("软件工程"));
        System.err.println(rankServiceImp.getCntOfClas("软件工程", 6));
        System.err.println(rankServiceImp.getCntOfGrade(2));
    }
    @Test
    public void TestA(){
        System.err.println(rankServiceImp.getCntOfMajorByWeek("软件工程"));
        System.err.println(rankServiceImp.getScoreOfMajorByWeek("软件工程"));
        System.err.println(rankServiceImp.getCntOfYearByDept(2023));
        System.err.println(rankServiceImp.getScoreOfYearByDept(2023));
    }

    @Test
    public void SpiderTest(){
        System.err.println(resourceService.getTextList());
    }
    @Test
    public void Testx(){
    }
}
