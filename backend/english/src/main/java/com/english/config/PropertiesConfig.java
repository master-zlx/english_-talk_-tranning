package com.english.config;

import java.io.FileInputStream;
import java.io.IOException;
import java.net.URISyntaxException;
import java.util.Properties;

public class PropertiesConfig {
    private static final String appId;
    private static final String apiKey;
    private static final String apiSecret;

    static {
        Properties properties = new Properties();
        try {
            properties.load(new FileInputStream(
                    PropertiesConfig.class.getResource("/").toURI().getPath() + "test.properties"));
        } catch (IOException | URISyntaxException e) {
            throw new RuntimeException("加载配置文件失败");
        }
        appId = properties.getProperty("appId");
        apiSecret = properties.getProperty("apiSecret");
        apiKey = properties.getProperty("apiKey");
    }

    public static String getAppId() {
        return appId;
    }

    public static String getApiKey() {
        return apiKey;
    }

    public static String getApiSecret() {
        return apiSecret;
    }
}
