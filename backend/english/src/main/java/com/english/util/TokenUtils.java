package com.english.util;

import cn.hutool.core.date.DateUtil;
import com.auth0.jwt.JWT;
import com.auth0.jwt.algorithms.Algorithm;
import com.english.domain.User;
import com.english.service.UserService;
import jakarta.annotation.Resource;
import org.springframework.stereotype.Component;

import java.util.Date;
@Component
public class TokenUtils {
    /**
     * 生成Token
     * @return
     */
    @Resource
    private UserService userService;
    public static String getToken(String userId,String sign){   //以password作为签名
        return JWT.create().withAudience(userId) // 将 user id 保存到 token 里面.作为载荷
                .withExpiresAt(DateUtil.offsetHour(new Date(),2)) //使用huttool里的util设置两小时过期
                .sign(Algorithm.HMAC256(sign)); // 以 password 作为 token 的密钥
    };
    public String getUsernameByToken(String token){
        String userid = JWT.decode(token).getAudience().get(0);
        User user = userService.getById(userid);
        return user.getUsername();
    }
}
