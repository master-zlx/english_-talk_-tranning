package com.english.service.impl;

import com.auth0.jwt.JWT;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.english.domain.Student;
import com.english.domain.User;
import com.english.domain.UserDTO;
import com.english.mappers.StudentMapper;
import com.english.mappers.UserMapper;
import com.english.service.UserService;
import com.english.util.TokenUtils;
import jakarta.annotation.Resource;
import org.springframework.stereotype.Service;

@Service
public class UserServiceImp implements UserService {
    @Resource
    UserMapper userMapper;
    @Resource
    StudentMapper studentMapper;
    @Override
    public User getById(String userId) {
        QueryWrapper qw = new QueryWrapper();
        qw.eq("userid", userId);
        User user = userMapper.selectOne(qw);
        return user;
    }

    private User getUserInfo(UserDTO userDTO) {
        String id = userDTO.getUserid();
        String word = userDTO.getPassword();
        QueryWrapper qw = new QueryWrapper();
        qw.eq("password", word);
        qw.eq("userid", id);
        User user = userMapper.selectOne(qw);
        return user;
    }
    private Student getStuByName(String name){
        QueryWrapper qw = new QueryWrapper<>();
        qw.eq("name", name);
        return studentMapper.selectOne(qw);
    }
    private void saveNewUser(User user){
        userMapper.insert(user);
    }
    private void saveNewStudent(Student student){
        studentMapper.insert(student);
    }
    @Override
    public UserDTO login(UserDTO userDTO) {
        User user = getUserInfo(userDTO);
        Student student = getStuByName(user.getUsername());
        if( user != null && student != null ){
            //把User属性copy给UserDto再返回给前端
            userDTO.setUser(user);
            userDTO.setStudent(student);
            String token = TokenUtils.getToken( String.valueOf(user.getUserid()), user.getPassword());
            userDTO.setToken(token);
            return userDTO;
        }
        return null;
    }
    @Override
    public UserDTO register(UserDTO userDTO) {
        // 检查用户名是否已存在
        if ( getStuByName(userDTO.getName()) != null || getById(userDTO.getUserid()) != null ) {
            // 用户已存在，返回错误信息
            return null;
        }
        // 创建用户并保存到数据库
        User newUser = new User();
        newUser.setUserid(userDTO.getUserid());
        newUser.setPassword(userDTO.getPassword());
        newUser.setUsername(userDTO.getName());
        Student newstudent = new Student();
        newstudent.setClas(userDTO.getClas());
        newstudent.setMajor(userDTO.getMajor());
        newstudent.setGrade(userDTO.getGrade());
        newstudent.setDept(userDTO.getDept());
        newstudent.setName(userDTO.getName());
        // 保存用户到数据库
        this.saveNewUser(newUser);
        this.saveNewStudent(newstudent);
        userDTO.setPassword(null);
        return userDTO;
    }

    @Override
    public UserDTO getInfo(String token) {
        User user = getById(JWT.decode(token).getAudience().get(0));
        Student student = getStuByName(user.getUsername());
        UserDTO userDTO = new UserDTO();
        userDTO.setUser(user);
        userDTO.setStudent(student);
        return userDTO;
    }

}
