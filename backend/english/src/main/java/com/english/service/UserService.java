package com.english.service;

import com.english.domain.User;
import com.english.domain.UserDTO;
public interface UserService {
    User getById(String userId);

    UserDTO login(UserDTO userDTO);

    UserDTO register(UserDTO userDTO);

    UserDTO getInfo(String token);
}
