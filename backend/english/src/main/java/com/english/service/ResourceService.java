package com.english.service;

import com.english.domain.LText;
import com.english.common.VideoList;
import com.english.domain.Text;

import java.util.List;

public interface ResourceService {
    public VideoList getVideo();
    public List<LText> getTextList();
    public Text getTextById(String id);

    LText getLTextById(String id);
}
