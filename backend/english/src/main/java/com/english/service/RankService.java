package com.english.service;

import com.english.common.RankResponse;
import org.apache.poi.ss.formula.functions.Rank;

import java.util.List;

public interface RankService {
    List<RankResponse> getTextByCnt();
    List<RankResponse> getDeptByCnt();
    List<RankResponse> getMajorByCnt();
    List<RankResponse> getClassByCnt();
    List<RankResponse> getGradeByCnt();
    List<RankResponse> getTextByScore();
    List<RankResponse> getDeptByScore();
    List<RankResponse> getMajorByScore();
    List<RankResponse> getClassByScore();
    List<RankResponse> getGradeByScore();
    int getCntOfWeek(String username, int week);
    int getCntOfMonth(String username, int month);
    int getCntOfTerm(String username, int term);
    int getCntOfYear(String username, int year);
    int getCntOfDept(String dept);
    int getCntOfMajor(String major);
    int getCntOfClas(String dept, int clas);
    int getCntOfGrade(int grade);

    Double getScoreOfWeek(String username, int week);
    Double getScoreOfMonth(String username, int month);
    Double getScoreOfTerm(String username, int term);
    Double getScoreOfYear(String username, int year);
    Double getScoreOfDept(String dept);
    Double getScoreOfMajor(String major);
    Double getScoreOfClas(String dept, int clas);
    Double getScoreOfGrade(int grade);
    List<RankResponse> getCntOfMajorByWeek(String major);
    List<RankResponse> getScoreOfMajorByWeek(String major);
    List<RankResponse> getCntOfYearByDept(int year);
    List<RankResponse> getScoreOfYearByDept(int year);
}