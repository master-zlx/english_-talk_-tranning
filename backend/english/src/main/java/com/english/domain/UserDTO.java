package com.english.domain;

import lombok.Data;

@Data
public class UserDTO {

    private String userid;
    private String password;
    private String token;
    private String name;
    private String dept;
    private String major;
    private String clas;
    private long grade;
    public UserDTO(String userid, String password){
        this.password = password;
        this.userid = userid;
    }

    public UserDTO() {

    }

    public void setUser(User user) {
        this.userid = user.getUserid();
        this.name = user.getUsername();
    }
    public void setStudent(Student student){
        this.name = student.getName();
        this.dept = student.getDept();
        this.major = student.getMajor();
        this.clas = student.getClas();
        this.grade = student.getGrade();
    }

    public boolean valid() {
        if(userid==null||password==null||name==null||dept==null||major==null|| clas==null|| grade < 1)
            return true;
        return false;
    }
}
