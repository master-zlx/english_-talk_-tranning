package com.english.domain;


import lombok.Data;

@Data
public class Student {

  private String name;
  private String dept;
  private String major;
  private String clas;
  private long grade;
}
