package com.english.domain;

public class Score {
    String accuracy_score;//准确度分
    String message="录音正常";//录音是否正确的信息
    String fluency_score;//流利度分
    String integrity_score;//完整度分
    String standard_score;//标准度分


    @Override
    public String toString() {
        return "Score{" +
                "accuracy_score='" + accuracy_score + '\'' +
                ", message='" + message + '\'' +
                ", fluency_score='" + fluency_score + '\'' +
                ", integrity_score='" + integrity_score + '\'' +
                ", standard_score='" + standard_score + '\'' +
                '}';
    }

    public Score(String accuracy_score, String message, String fluency_score, String integrity_score, String standard_score) {
        this.accuracy_score = accuracy_score;
        this.message = message;
        this.fluency_score = fluency_score;
        this.integrity_score = integrity_score;
        this.standard_score = standard_score;
    }

    public Score() {
    }

    public String getAccuracy_score() {
        return accuracy_score;
    }

    public void setAccuracy_score(String accuracy_score) {
        this.accuracy_score = accuracy_score;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        if (message.equals("28673"))
            this.message = "无语音或音量小";
//        if (message.equals("28676"))
//            this.message = "乱说";
        if (message.equals("28680"))
            this.message = "信噪比低";
        if (message.equals("28690"))
            this.message = "截幅";
        if (message.equals("28689"))
            this.message = "没有音频输入";
    }

    public String getFluency_score() {
        return fluency_score;
    }

    public void setFluency_score(String fluency_score) {
        this.fluency_score = fluency_score;
    }

    public String getIntegrity_score() {
        return integrity_score;
    }

    public void setIntegrity_score(String integrity_score) {
        this.integrity_score = integrity_score;
    }

    public String getStandard_score() {
        return standard_score;
    }

    public void setStandard_score(String standard_score) {
        this.standard_score = standard_score;
    }
}
