package com.english.domain;

import com.english.domain.Text;
import lombok.Data;

import java.util.ArrayList;
import java.util.List;

//此类为Text中content改为链表，用于返回适应前端的数据
@Data
public class LText{
    private String id;
    private String etitle;
    private String ctitle;
    private List<String> content;
    private String titlepic;
    public LText(Text text){
        setId(text.getId());
        setCtitle(text.getCtitle());
        setEtitle(text.getEtitle());
        setTitlepic(text.getTitlepic());
    }

    public void setContentList(String str) {
        content = new ArrayList<>();
        for (int i = 0; i < str.length(); i++){
            if(str.substring(i, i + 1).equals("#")){
                i++;
                int lenth = 0;
                while( i + lenth + 1< str.length() && !str.substring(i + lenth, i + lenth + 1).equals("#")){
                    lenth++;
                }
                if( lenth == 0)continue;
                content.add( str.substring(i, i + lenth));
            }
        }
    }
}
