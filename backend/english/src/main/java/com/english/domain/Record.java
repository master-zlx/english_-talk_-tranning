package com.english.domain;

import lombok.Data;

@Data
public class Record {
  private String name;
  private String id;
  private double score;
  private long week;
}
