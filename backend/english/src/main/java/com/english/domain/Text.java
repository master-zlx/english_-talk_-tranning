package com.english.domain;

import com.baomidou.mybatisplus.annotation.TableField;
import lombok.Data;

@Data
public class Text {
  private String id;
  private String etitle;
  private String ctitle;
  private String content;
  private String titlepic;

}
