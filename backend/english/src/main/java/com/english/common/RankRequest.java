package com.english.common;

import lombok.Data;

@Data
public class RankRequest {
    private String param1;
    private String param2;
}
