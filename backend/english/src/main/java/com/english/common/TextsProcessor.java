package com.english.common;

import com.english.domain.Text;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.Data;
import us.codecraft.webmagic.Page;
import us.codecraft.webmagic.Site;
import us.codecraft.webmagic.Spider;
import us.codecraft.webmagic.processor.PageProcessor;
import us.codecraft.webmagic.selector.Json;

import java.util.ArrayList;
import java.util.List;
@Data
public class TextsProcessor implements PageProcessor {
    private List<Text> textList;
    private Site site = Site.me().setRetryTimes(3).setSleepTime(100);
    @Override
    public void process(Page page) {
        Json json = page.getJson();
        //处理json, 取出数据
        try {
            ObjectMapper objectMapper = new ObjectMapper();
            JsonNode jsonNode = objectMapper.readTree(String.valueOf(json));
            JsonNode objectsNode = jsonNode.get("objects");
            for (JsonNode node : objectsNode){
                Text text = new Text();
                text.setId( node.get("id").asText() );
                text.setEtitle( node.get("title_en").asText() );
                text.setCtitle( node.get("title_cn").asText() );
                text.setTitlepic( node.get("thumbnail_url").asText() );
                text.setContent( this.getTextById( text.getId() ));
                this.textList.add(text);
            }
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    private String getTextById(String id) {
        return new TextContentProcessor(id).getContent();
    }
    public TextsProcessor(String textsUrl) {
        this.textList = new ArrayList<>();
        site.addCookie("auth_token", "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6MjUyNTg1OTM5LCJleHAiOjE2ODk5MjI5NDQsImV4cF92MiI6MTY4OTkyMjk0NCwiZGV2aWNlIjoiIiwidXNlcm5hbWUiOiJXZWNoYXRfOWI5ODUyY2U2YzYzZDg4MCIsImlzX3N0YWZmIjowLCJzZXNzaW9uX2lkIjoiMzA2MmU5OTgxZmI4MTFlZTg4M2ZlMmVhZDA3N2FlNDMifQ.1Kg_00QpCzfvCCxLm7DhIYLBpPKST-VDb8b8OAKxizQ");
        Spider.create(this).addUrl(textsUrl).thread(6).run();
    }
}