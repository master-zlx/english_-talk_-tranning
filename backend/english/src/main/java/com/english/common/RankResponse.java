package com.english.common;

import lombok.Data;

@Data
public class RankResponse {
    String attribute;
    private Integer times;
    private Double score;
    public RankResponse(String attribute, Integer times, Double score){
        this.attribute = attribute;
        this.times = times;
        if(score != null)
            this.score = Math.round(score * 100.0) / 100.0;
    }
}
