package com.english.common;

import java.util.TreeMap;

/**
 * 用于存放平均分数的红黑树
 */
public class AvgTreeMap extends TreeMap {
    private TreeMap<String, Integer> numOfValue = new TreeMap<>();
    private TreeMap<String, Double> treeMap = new TreeMap<>();

    public void update(String key, Double score){
        Double new_score = score;
        if( treeMap.get(key) != null && numOfValue.get(key) != null ){
            new_score = (score + treeMap.get(key) * numOfValue.get(key)) / (numOfValue.get(key) + 1 );
        };
        this.treeMap.put(key, new_score);
        if( numOfValue.get(key) == null )
                numOfValue.put(key, 1);  // numOfValue[key]++;
        else {
            numOfValue.put(key, 1 + numOfValue.get(key));
        }
    }

    public TreeMap getTreeMap(){
        if(this.treeMap == null)return null;
        return this.treeMap;
    }
}
