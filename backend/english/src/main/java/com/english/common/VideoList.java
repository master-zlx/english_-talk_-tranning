package com.english.common;


import lombok.Data;

import java.util.List;
@Data
public class VideoList{
    private List<String> urls;
    private List<String > titles;

    public VideoList(List<String> list1, List<String> list2){
        urls = list1;
        titles = list2;
    }
}
