package com.english.controller;

import com.english.common.CommonResponse;
import com.english.domain.LText;
import com.english.common.VideoList;
import com.english.service.ResourceService;
import jakarta.annotation.Resource;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("homepage")
public class HomePageController {
    @Resource
    ResourceService resourceService;
    @GetMapping("/video")
    public CommonResponse getVideo(){
        VideoList urlList= resourceService.getVideo();
        if( urlList != null ){
            return CommonResponse.createForSuccess(urlList);
        }
        else return CommonResponse.createForError("没有视频资源");
    }
    @GetMapping("/text")
    public CommonResponse getText(){
        List<LText> lText = resourceService.getTextList();
        if( lText != null ){
            return CommonResponse.createForSuccess(lText);
        }
        else return CommonResponse.createForError("没有文章资源");
    }
}
