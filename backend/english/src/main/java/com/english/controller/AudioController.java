package com.english.controller;


import com.english.domain.Score;
import com.english.service.IseDemo;
import com.english.service.TtsDemo;
import jakarta.servlet.http.HttpServletResponse;
import org.apache.tomcat.util.http.fileupload.IOUtils;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.io.OutputStream;


@RestController
@RequestMapping("/audio")
public class AudioController {



//
//    @GetMapping("/test")
//    public void test(@RequestParam String txt) throws Exception {
//        TtsDemo.test(txt);
//
//    }

//    播放对应文本的音频
    @GetMapping
    public void playAudio(@RequestParam String txt,@RequestParam String speaker,HttpServletResponse response) throws Exception {
        TtsDemo.test(txt,speaker);
        String FilePath= TtsDemo.PATH;//文件路径
        response.setCharacterEncoding("utf-8");
        response.setContentType("multipart/form-data");
        response.setHeader("Content-Disposition", "123.mp3");//文件名
        response.setHeader("Access-Control-Expose-Headers","Content-Disposition");
        File file = new File(FilePath);
        if(file.exists()&&file.isFile()){
            try {
                InputStream inputStream = new FileInputStream(file);
                OutputStream outputStream = response.getOutputStream();//获取response的输出流对象
                IOUtils.copy(inputStream,outputStream);
                //刷新输出流
                outputStream.flush();
                //关闭输出流
                outputStream.close();
                inputStream.close();
            }catch (Exception e){
                e.printStackTrace();
            }
        }
    }

//    SimpleDateFormat sdf = new SimpleDateFormat("yyyy/MM/dd");
//    @PostMapping("/upload")
//    public String upload(MultipartFile multipartfile, HttpServletRequest request){
//        String realPath = request.getSession().getServletContext().getRealPath("/update/");
////        System.out.println(realPath);
//        String format=sdf.format(new Date());
//        File folder=new File(realPath+format);
//        System.out.println(realPath+format);
//        if(!folder.isDirectory()){
//            folder.mkdirs();
//        }
//        String oldname = multipartfile.getOriginalFilename();
//        String newname = UUID.randomUUID().toString()+oldname.substring(oldname.lastIndexOf("."),oldname.length());
//        try {
//            multipartfile.transferTo(new File(folder, newname));
//            System.out.println(new File(folder, newname).getAbsolutePath());//输出（上传文件）保存的绝对路径
//            String filePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+"/update/"+format+newname;
//            return filePath+"上传成功";
//        }
//        catch (IOException e){
//            e.printStackTrace();
//        }
//        return "上传失败!";
//    }

    @PostMapping("/score")
    public Score uploadFile(@RequestParam("file") MultipartFile file, @RequestParam("txt") String txt) throws Exception {
        String fileName = file.getOriginalFilename();
        String suffix = fileName.substring(fileName.lastIndexOf('.'));
        String newFileName = "record" + suffix;
        String path = "E:/test/";
        File newFile = new File(path + newFileName);
        file.transferTo(newFile);
        IseDemo.getScore(txt);
        Thread.sleep(5000);
        System.out.println(IseDemo.score);
        return IseDemo.score;
    }

    //给对应音频打分】
    @GetMapping("/score")
    public Score getScore() throws Exception {
        IseDemo.getScore("hello hello hello");
        Thread.sleep(2000);
        System.out.println(IseDemo.score);
        return IseDemo.score;
    }

}



