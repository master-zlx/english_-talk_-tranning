package com.english.controller;

import com.english.common.CommonResponse;
import com.english.common.RankRequest;
import com.english.service.RankService;
import com.english.util.TokenUtils;
import jakarta.annotation.Resource;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("rank")
public class RankPageController {
    @Resource
    RankService rankService;
    @Resource
    TokenUtils tokenUtils;
    @PostMapping("/top")
    public CommonResponse<List> getRankByKindAndWay(@RequestBody RankRequest request) {
        String kind = request.getParam1();
        String way = request.getParam2();
        if( way.equals("times") ){
            if(kind.equals("text")){
                return CommonResponse.createForSuccess(rankService.getTextByCnt());
            }
            else if(kind.equals("dept")){
                return CommonResponse.createForSuccess(rankService.getDeptByCnt());
            }
            else if(kind.equals("major")){
                return CommonResponse.createForSuccess(rankService.getMajorByCnt());
            }
            else if(kind.equals("clas")){
                return CommonResponse.createForSuccess(rankService.getClassByCnt());
            }
            else if(kind.equals("grade")){
                return CommonResponse.createForSuccess(rankService.getGradeByCnt());
            }
        }
        else if(way.equals("score")){
            if(kind.equals("text")){
                return CommonResponse.createForSuccess(rankService.getTextByScore());
            }
            else if(kind.equals("dept")){
                return CommonResponse.createForSuccess(rankService.getDeptByScore());
            }
            else if(kind.equals("major")){
                return CommonResponse.createForSuccess(rankService.getMajorByScore());
            }
            else if(kind.equals("clas")){
                return CommonResponse.createForSuccess(rankService.getClassByScore());
            }
            else if(kind.equals("grade")){
                return CommonResponse.createForSuccess(rankService.getGradeByScore());
            }
        }
        return CommonResponse.createForError();
    }
    @PostMapping("/count/week/{week}")
    public CommonResponse getCntByWeek(
            @RequestHeader("token")String token, @PathVariable("week") int week){
        if(week < 1 || week > 32){
            return CommonResponse.createForError("传入日期错误");
        }
        String username = tokenUtils.getUsernameByToken(token);
        return CommonResponse.createForSuccess("SUCCESS",rankService.getCntOfWeek(username, week));
    }
    @PostMapping("/count/month/{month}")
    public CommonResponse getCntByMonth(
            @RequestHeader("token")String token, @PathVariable("month") int month){
        if(month < 1 || month > 12){
            return CommonResponse.createForError("传入日期错误");
        }
        String username = tokenUtils.getUsernameByToken(token);
        return CommonResponse.createForSuccess("SUCCESS",rankService.getCntOfMonth(username, month));
    }
    @PostMapping("/count/term/{term}")
    public CommonResponse getCntByTerm(
            @RequestHeader("token")String token, @PathVariable("term") int term){
        if(term < 1 || term > 2){
            return CommonResponse.createForError("传入日期错误");
        }
        String username = tokenUtils.getUsernameByToken(token);
        return CommonResponse.createForSuccess("SUCCESS",rankService.getCntOfTerm(username, term));
    }
    @PostMapping("/count/year/{year}")
    public CommonResponse getCntByYear(
            @RequestHeader("token")String token, @PathVariable("year") int year){
        String username = tokenUtils.getUsernameByToken(token);
        return CommonResponse.createForSuccess("SUCCESS",rankService.getCntOfYear(username, year));
    }
    @PostMapping("/count/dept")
    public CommonResponse getCntByDept(@RequestBody RankRequest request){
        String dept = request.getParam1();
        if(dept == null){
            return CommonResponse.createForError("参数为空");
        }
        return CommonResponse.createForSuccess("SUCCESS",rankService.getCntOfDept(dept));
    }
    @PostMapping("/count/major")
    public CommonResponse getCntByMajor(@RequestBody RankRequest request){
        String major = request.getParam1();
        if(major == null){
            return CommonResponse.createForError("参数为空");
        }
        return CommonResponse.createForSuccess("SUCCESS",rankService.getCntOfMajor(major));
    }
    @PostMapping("/count/clas")
    public CommonResponse getCntByClas(@RequestBody RankRequest request){
        String dept = request.getParam1();
        int clas = Integer.parseInt(request.getParam2());
        if(dept == null || clas < 1 ){
            return CommonResponse.createForError("参数错误");
        }
        return CommonResponse.createForSuccess("SUCCESS",rankService.getCntOfClas(dept, clas));
    }
    @PostMapping("/count/grade/{grade}")
    public CommonResponse getCntByDept(@PathVariable("grade") int grade){
        if( grade < 1 || grade > 4){
            return CommonResponse.createForError("参数错误");
        }
        return CommonResponse.createForSuccess("SUCCESS",rankService.getCntOfGrade(grade));
    }

    /**
     * 👇Score👇
     */
    @PostMapping("/score/week/{week}")
    public CommonResponse getScoreByWeek(@RequestHeader("token")String token,
                                         @PathVariable("week") int week){
        if(week < 1 || week > 32){
            return CommonResponse.createForError("传入日期错误");
        }
        String username = tokenUtils.getUsernameByToken(token);
        return CommonResponse.createForSuccess("SUCCESS",rankService.getScoreOfWeek(username,week));
    }

    @PostMapping("/score/month/{month}")
    public CommonResponse getScoreByMonth(@RequestHeader("token")String token,
                                          @PathVariable("month") int month){
        if(month < 1 || month > 12){
            return CommonResponse.createForError("传入日期错误");
        }
        String username = tokenUtils.getUsernameByToken(token);
        return CommonResponse.createForSuccess("SUCCESS",rankService.getScoreOfMonth(username, month));
    }

    @PostMapping("/score/term/{term}")
    public CommonResponse getScoreByTerm(@RequestHeader("token")String token,
                                         @PathVariable("term") int term){
        if(term < 1 || term > 2){
            return CommonResponse.createForError("传入日期错误");
        }
        String username = tokenUtils.getUsernameByToken(token);
        return CommonResponse.createForSuccess("SUCCESS",rankService.getScoreOfTerm(username, term));
    }

    @PostMapping("/score/year/{year}")
    public CommonResponse getScoreByYear(@RequestHeader("token")String token,
                                         @PathVariable("year") int year){
        String username = tokenUtils.getUsernameByToken(token);
        return CommonResponse.createForSuccess("SUCCESS",rankService.getScoreOfYear(username, year));
    }
    @PostMapping("/score/dept")
    public CommonResponse getScoreByDept(@RequestBody RankRequest request){
        String dept = request.getParam1();
        if(dept == null){
            return CommonResponse.createForError("参数为空");
        }
        return CommonResponse.createForSuccess("SUCCESS",rankService.getScoreOfDept(dept));
    }

    @PostMapping("/score/major")
    public CommonResponse getScoreByMajor(@RequestBody RankRequest request){
        String major = request.getParam1();
        if(major == null){
            return CommonResponse.createForError("参数为空");
        }
        return CommonResponse.createForSuccess("SUCCESS",rankService.getScoreOfMajor(major));
    }
    @PostMapping("/score/clas")
    public CommonResponse getScoreByClas(@RequestBody RankRequest request){
        String dept = request.getParam1();
        int clas = Integer.parseInt(request.getParam2());
        if(dept == null || clas < 1 ){
            return CommonResponse.createForError("参数错误");
        }
        return CommonResponse.createForSuccess("SUCCESS",rankService.getScoreOfClas(dept, clas));
    }
    @PostMapping("/score/grade/{grade}")
    public CommonResponse getScoreByDept(@PathVariable("grade") int grade){
        if( grade < 1 || grade > 4){
            return CommonResponse.createForError("参数错误");
        }
        return CommonResponse.createForSuccess("SUCCESS",rankService.getScoreOfGrade(grade));
    }

    @PostMapping("/count/total/major")
    public CommonResponse getCntOfMajorByWeek(@RequestBody RankRequest request){
        String major = request.getParam1();
        return CommonResponse.createForSuccess(rankService.getCntOfMajorByWeek(major));
    }
    @PostMapping("/score/total/major")
    public CommonResponse getScoreOfMajorByWeek(@RequestBody RankRequest request){
        String major = request.getParam1();
        return CommonResponse.createForSuccess(rankService.getScoreOfMajorByWeek(major));
    }
    @PostMapping("/count/total/year/{year}")
    public CommonResponse getCntOfYearByDept(@PathVariable("year") int year){
        return CommonResponse.createForSuccess(rankService.getCntOfYearByDept(year));
    }
    @PostMapping("/score/total/year/{year}")
    public CommonResponse getScoreOfYearByDept(@PathVariable("year") int year){
        return CommonResponse.createForSuccess(rankService.getScoreOfYearByDept(year));
    }

}
