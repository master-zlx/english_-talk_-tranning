package com.english.controller;

import com.english.common.CommonResponse;
import com.english.domain.LText;
import com.english.service.ResourceService;
import jakarta.annotation.Resource;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("text")
public class TextPageController {
    @Resource
    ResourceService resourceService;
    @GetMapping("/{id}")
    public CommonResponse getText(@PathVariable("id") String id){
        LText lText = resourceService.getLTextById(id);
        if( lText == null )return CommonResponse.createForError("不存在");
        else
        {
            return CommonResponse.createForSuccess(lText);
        }
    }
}
