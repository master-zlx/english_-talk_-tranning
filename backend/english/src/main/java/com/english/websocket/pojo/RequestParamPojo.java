package com.english.websocket.pojo;

public class RequestParamPojo {
    /**
     * 传输文本
     */
    private byte[] byteContent;

    /**
     * 数据大小
     */
    private Integer dataLength;

    /**
     * 数据传输状态
     */
    private Integer status;


    public byte[] getByteContent() {
        return byteContent;
    }

    public void setByteContent(byte[] byteContent) {
        this.byteContent = byteContent;
    }

    public Integer getDataLength() {
        return dataLength;
    }

    public void setDataLength(Integer dataLength) {
        this.dataLength = dataLength;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }
}
