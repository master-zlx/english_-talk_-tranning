package com.english.websocket.dealer;

import com.alibaba.fastjson.JSONObject;

import java.util.Map;

public interface RequestDataDealer {
    void collectData(JSONObject originalData, Map<String, String> requestPath, String appId) throws Exception;
}
