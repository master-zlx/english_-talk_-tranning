package com.english.websocket.queue;

import com.alibaba.fastjson.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;

public class QueueManage {
    private static final Logger logger = LoggerFactory.getLogger(QueueManage.class);
    private static final BlockingQueue<JSONObject> websocketRequestParamQueue = new LinkedBlockingQueue<>(128);

    private static final QueueManage INSTANCE = new QueueManage();

    public static QueueManage getInstance() {
        return INSTANCE;
    }

    public void productRequestParamData(JSONObject requestParamData) {
        try {
            if (requestParamData != null) {
                websocketRequestParamQueue.put(requestParamData);
            }
        } catch (InterruptedException e) {
            logger.error("Product requestParamData to queue failed ", e);
        }
    }

    public JSONObject consumeRequestParamData() throws InterruptedException {
        return websocketRequestParamQueue.take();
    }
}
