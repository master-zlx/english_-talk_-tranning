package com.english.websocket.constants;

public class ProjectConstants {
    /**
     * 流式发送状态status
     */
    public static final int STATUS_BEGIN = 0;
    public static final int STATUS_CONTINUE = 1;
    public static final int STATUS_END = 2;

    /**
     * 媒体类型
     */
    public static final String TEXT = "text";
    public static final String IMAGE = "image";
    public static final String VIDEO = "video";
    public static final String AUDIO = "audio";
}
