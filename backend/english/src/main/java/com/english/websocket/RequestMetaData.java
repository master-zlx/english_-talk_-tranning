package com.english.websocket;

import com.alibaba.fastjson.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class RequestMetaData {
    private static final String hostUrl = "wss://cn-east-1.ws-api.xf-yun.com/v1/private/s8e098720";
    private static final Map<String, String> mediaTypePathMap = new HashMap<>();
    private static final List<String> responsePathList = new ArrayList<>();

    static {
        mediaTypePathMap.put("$.payload.data.audio", "input/您好，欢迎来到科大讯飞.mp3");
        responsePathList.add("$.payload.result");
    }

    public static Map<String, String> getRequestPathMap() {
        return mediaTypePathMap;
    }

    public static String getHostUrl() {
        return hostUrl;
    }

    public static List<String> getResponsePathList() {
        return responsePathList;
    }

    /**
    * 获取JsonObject形式的请求协议
    *
    */
    public static JSONObject getJsonObjectRequestData() {
        JSONObject jsonObjectRequestData = new JSONObject();
        JSONObject header = new JSONObject();
        jsonObjectRequestData.put("header", header);
        header.put("app_id", "123456");
        header.put("status", 0);
        JSONObject payload = new JSONObject();
        jsonObjectRequestData.put("payload", payload);
        JSONObject data = new JSONObject();
        payload.put("data", data);
        data.put("sample_rate", 16000);
        data.put("channels", 1);
        data.put("audio", "");
        data.put("encoding", "speex-wb");
        data.put("bit_depth", 16);
        data.put("frame_size", 0);
        data.put("seq", 0);
        data.put("status", 0);
        JSONObject parameter = new JSONObject();
        jsonObjectRequestData.put("parameter", parameter);
        JSONObject st = new JSONObject();
        parameter.put("st", st);
        st.put("dict_type", "");
        st.put("getParam", 0);
        st.put("customized_lexicon", "");
        st.put("refText", "");
        st.put("precision", 0);
        st.put("paragraph_need_word_score", 0);
        st.put("scale", 0);
        st.put("seek", 0);
        st.put("serverTimeout", 0);
        st.put("realtime_feedback", 0);
        JSONObject result = new JSONObject();
        st.put("result", result);
        result.put("format", "plain");
        result.put("encoding", "utf8");
        result.put("compress", "raw");
        st.put("core", "word");
        st.put("phoneme_diagnosis", 0);
        st.put("attachAudioUrl", 0);
        st.put("refPinyin", "");
        st.put("slack", 0);
        st.put("vad", 0);
        st.put("ref_length", 0);
        st.put("phoneme_output", 0);
        st.put("lang", "cn");
        st.put("output_rawtext", 0);
        return jsonObjectRequestData;
    }

    /**
    * 获取Json字符串形式的请求协议
    *
    */
    public static String getJsonStringRequestData() {
        String jsonStringRequestData = "{\n" +
                "    \"header\": {\n" +
                "        \"app_id\": \"123456\",\n" +
                "        \"status\": 0\n" +
                "    },\n" +
                "    \"payload\": {\n" +
                "        \"data\": {\n" +
                "            \"sample_rate\": 16000,\n" +
                "            \"channels\": 1,\n" +
                "            \"audio\": \"\",\n" +
                "            \"encoding\": \"lame\",\n" +
                "            \"bit_depth\": 16,\n" +
                "            \"frame_size\": 0,\n" +
                "            \"seq\": 0,\n" +
                "            \"status\": 0\n" +
                "        }\n" +
                "    },\n" +
                "    \"parameter\": {\n" +
                "        \"st\": {\n" +
                "            \"lang\": \"cn\",\n" +
                "            \"core\": \"para\",\n" +
                "            \"refText\": \"您好，欢迎来到科大讯飞。\",\n" +
                "            \"result\": {\n" +
                "                \"format\": \"plain\",\n" +
                "                \"encoding\": \"utf8\",\n" +
                "                \"compress\": \"raw\"\n" +
                "            },\n" +
//                "            \"dict_type\": \"\",\n" +
//                "            \"getParam\": 0,\n" +
//                "            \"customized_lexicon\": \"\",\n" +
//                "            \"precision\": 0,\n" +
//                "            \"paragraph_need_word_score\": 0,\n" +
//                "            \"scale\": 0,\n" +
//                "            \"seek\": 0,\n" +
//                "            \"serverTimeout\": 0,\n" +
//                "            \"realtime_feedback\": 0,\n" +
//                "            \"phoneme_diagnosis\": 0,\n" +
//                "            \"attachAudioUrl\": 0,\n" +
//                "            \"refPinyin\": \"\",\n" +
//                "            \"slack\": 0,\n" +
//                "            \"vad\": 0,\n" +
//                "            \"ref_length\": 0,\n" +
//                "            \"phoneme_output\": 0,\n" +
//                "            \"output_rawtext\": 0\n" +
                "        }\n" +
                "    }\n" +
                "}";
        return jsonStringRequestData;
    }
}