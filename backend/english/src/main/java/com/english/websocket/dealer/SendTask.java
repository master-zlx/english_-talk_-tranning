package com.english.websocket.dealer;

import com.alibaba.fastjson.JSONObject;
import com.english.websocket.AppClient;
import com.english.websocket.queue.QueueManage;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class SendTask implements Runnable{
    private static final Logger logger = LoggerFactory.getLogger(SendTask.class);
    private AppClient appClient;
    @Override
    public void run() {
        while (true) {
            try {
                logger.info("start consume from queue");
                JSONObject requestParamData = QueueManage.getInstance().consumeRequestParamData();
                logger.info("consume requestParamData is:{}", requestParamData.toString());
                this.appClient.getWebSocket().send(requestParamData.toString());
                JSONObject header = requestParamData.getJSONObject("header");
                if (header == null) {
                    return;
                }
                Integer status = header.getInteger("status");
                if (status == null || status == 2) {
                    return;
                }
            } catch (InterruptedException e) {
                logger.error("Consume RequestParamData from queue error, return send thread", e);
                return;
            }
        }
    }

    public void setAppClient(AppClient appClient) {
        this.appClient = appClient;
    }
}
