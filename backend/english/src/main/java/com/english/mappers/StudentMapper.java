package com.english.mappers;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.english.domain.Student;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface StudentMapper extends BaseMapper<Student> {
}
