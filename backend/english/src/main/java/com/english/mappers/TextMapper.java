package com.english.mappers;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.english.domain.Text;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface TextMapper extends BaseMapper<Text> {
}
