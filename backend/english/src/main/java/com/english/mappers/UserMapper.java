package com.english.mappers;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.english.domain.User;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface UserMapper extends BaseMapper<User> {
}
