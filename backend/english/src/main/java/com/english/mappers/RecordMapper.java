package com.english.mappers;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.english.domain.Record;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

import java.util.Collection;
import java.util.List;

@Mapper
public interface RecordMapper extends BaseMapper<Record> {
    @Select("SELECT *\n" +
            "FROM record\n" +
            "WHERE record.`name` IN (SELECT student.`name` FROM student WHERE student.dept = '${dept}');\n")
    List<Record> getByDept(@Param("dept")String dept);
    @Select("SELECT *\n" +
            "FROM record\n" +
            "WHERE record.`name` IN (SELECT student.`name` FROM student WHERE student.major = '${major}');\n")
    List<Record> getByMajor(@Param("major")String major);
    @Select("SELECT *\n" +
            "FROM record\n" +
            "WHERE record.`name` IN (SELECT student.`name` FROM student WHERE student.major = '${major}' and student.clas = '${clas}');\n")
    List<Record> getByClas(String major, int clas);
    @Select("SELECT *\n" +
            "FROM record\n" +
            "WHERE record.`name` IN (SELECT student.`name` FROM student WHERE student.grade = '${grade}');\n")
    List<Record> getByGrade(@Param("grade")int grade);

    @Select("SELECT *\n" +
            "FROM record\n" +
            "WHERE record.`name` IN (SELECT student.`name` FROM student WHERE student.major = '${major}' and record.week = '${week}');\n")
    List<Record> getByMajorAndWeek(@Param("major") String major,
                                    @Param("week") int week);
}
